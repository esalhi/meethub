$(document).ready(function () {
    $('.animated').appear(function () {
        var elem = $(this);
        var animation = elem.data('animation');
        if (!elem.hasClass('visible')) {
            var animationDelay = elem.data('animation-delay');
            if (animationDelay) {
                setTimeout(function () {
                    elem.addClass(animation + " visible");
                }, animationDelay);
            } else {
                elem.addClass(animation + " visible");
            }
        }
    });

    $('html,body').scrollTop(0);
    $('[data-toggle="tooltip"]').tooltip();
});

function target(item) {
    if ($("html").is(":animated")){ $("html").stop(); }
    return $("html, body").animate({
        scrollTop: $(item).offset().top + -$(".navbar").eq(0).height()
    }, 1000);
};
