﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeetHub.Models
{
    public class RechercheViewModel
    {
        public string Recherche { get; set; }

        public RechercheViewModel()
        {
            this.Recherche = "";
        }
    }
}
