﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeetHub.Models
{
    public class GroupeViewModel
    {
        public int GroupeId { get; set; }
        public string Nom { get; set; }
        public string Lieu { get; set; }
        public string Description { get; set; }
        public string DateCreation { get; set; }
        public string NbMembres { get; set; }
        public string NbEvenements { get; set; }
        public ApplicationUser Createur { get; set; }
    }

    public class GroupeGlobalViewModel
    {
        [Required]
        [Display(Name = "Nom")]
        public string AjouterNom { get; set; }

        [Required]
        [Display(Name = "Quartier général")]
        public string AjouterLieu { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string AjouterDescription { get; set; }

        public List<GroupeViewModel> groupes { get; set; }
        public List<GroupeViewModel> invitations { get; set; }
        public List<GroupeViewModel> groupesrecents { get; set; }

        public GroupeGlobalViewModel()
        {
            this.groupes = new List<GroupeViewModel>();
            this.invitations = new List<GroupeViewModel>();
            this.groupesrecents = new List<GroupeViewModel>();
        }
    }
}
