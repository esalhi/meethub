﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeetHub.Models
{
    public class EvenementViewModel
    {
        public int EvenementId { get; set; }
        public string Nom { get; set; }
        public string Lieu { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string NbMembres { get; set; }
        public Groupe Groupe { get; set; }
        public ApplicationUser Createur { get; set; }
    }

    public class EvenementGlobalViewModel
    {
        [Required]
        [Display(Name = "Nom")]
        public string AjouterNom { get; set; }

        [Required]
        [Display(Name = "Quartier général")]
        public string AjouterLieu { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string AjouterDescription { get; set; }

        [Required]
        [Display(Name = "Date de l'évènement")]
        public string AjouterDate { get; set; }

        public List<EvenementViewModel> evenements { get; set; }
        public List<EvenementViewModel> invitations { get; set; }
        public List<EvenementViewModel> evenementsrecents { get; set; }

        public EvenementGlobalViewModel()
        {
            this.evenements = new List<EvenementViewModel>();
            this.invitations = new List<EvenementViewModel>();
            this.evenementsrecents = new List<EvenementViewModel>();
        }
    }
}
