﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Validation;
using System.Text;
using System.Collections.Generic;

namespace MeetHub.Models
{
    // Vous pouvez ajouter des données de profil pour l'utilisateur en ajoutant plus de propriétés à votre classe ApplicationUser ; consultez http://go.microsoft.com/fwlink/?LinkID=317594 pour en savoir davantage.
    public class ApplicationUser : IdentityUser
    {
        public ICollection<Groupe> Groupes { get; set; }
        public ICollection<Evenement> Evenements { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Notez qu'authenticationType doit correspondre à l'élément défini dans CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Ajouter les revendications personnalisées de l’utilisateur ici
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Groupe> Groupes { get; set; }
        public DbSet<Evenement> Evenements { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(new DropCreateDatabaseIfModelChanges<ApplicationDbContext>());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>()
                .HasMany<Groupe>(u => u.Groupes)
                .WithMany(g => g.Membres)
                .Map(gu =>
                {
                    gu.MapLeftKey("ApplicationUserId");
                    gu.MapRightKey("GroupeId");
                    gu.ToTable("UserGroupe");
                });

            modelBuilder.Entity<ApplicationUser>()
                .HasMany<Evenement>(u => u.Evenements)
                .WithMany(e => e.Membres)
                .Map(ue =>
                {
                    ue.MapLeftKey("ApplicationUserId");
                    ue.MapRightKey("GroupeId");
                    ue.ToTable("UserEvenement");
                });
        }
        public override int SaveChanges(){
            try{
                return base.SaveChanges();
            }catch(DbEntityValidationException ex){
                var sb = new StringBuilder();

                foreach(var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach(var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                    );
            }
        }
    }
}