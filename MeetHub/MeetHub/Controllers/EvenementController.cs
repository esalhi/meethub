﻿using MeetHub.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetHub.Controllers
{
    public class EvenementController : Controller
    {
        public ActionResult AfficherEvenements()
        {
            using (var context = new ApplicationDbContext())
            {
                var UserId = User.Identity.GetUserId();
                var ListeMesEvenements = context.Evenements
                    .Where(evenement => UserId == evenement.Createur.Id)
                    .Select(evenement => new
                    {
                        EvenementId = evenement.EvenementId,
                        Nom = evenement.Nom,
                        Lieu = evenement.Lieu,
                        Description = evenement.Description,
                        Date = evenement.Date,
                        Membres = evenement.Membres,
                        Createur = evenement.Createur
                    });

                var ListeParticipationEvenements = context.Evenements
                    .Where(evenement => evenement.Membres.Any(membre => membre.Id == UserId))
                    .Select(evenement => new
                    {
                        EvenementId = evenement.EvenementId,
                        Nom = evenement.Nom,
                        Lieu = evenement.Lieu,
                        Description = evenement.Description,
                        Date = evenement.Date,
                        Membres = evenement.Membres,
                        Createur = evenement.Createur
                    });

                var ListeEvenementsRecents = context.Evenements
                    .Where(evenement => !evenement.Membres.Any(membre => membre.Id == UserId))
                    .Select(evenement => new
                    {
                        EvenementId = evenement.EvenementId,
                        Nom = evenement.Nom,
                        Lieu = evenement.Lieu,
                        Description = evenement.Description,
                        Date = evenement.Date,
                        Membres = evenement.Membres,
                        Createur = evenement.Createur
                    });

                ViewBag.Message = "Evenements";
                var viewModel = new EvenementGlobalViewModel();
                foreach (var e in ListeMesEvenements)
                {
                    viewModel.evenements.Add(new EvenementViewModel
                    {
                        EvenementId = e.EvenementId,
                        Nom = e.Nom,
                        Lieu = e.Lieu,
                        Date = e.Date,
                        Description = e.Description,
                        NbMembres = e.Membres.Count().ToString(),
                        Createur = e.Createur
                    });
                }

                foreach (var e in ListeParticipationEvenements)
                {
                    viewModel.invitations.Add(new EvenementViewModel
                    {
                        EvenementId = e.EvenementId,
                        Nom = e.Nom,
                        Lieu = e.Lieu,
                        Date = e.Date,
                        Description = e.Description,
                        NbMembres = e.Membres.Count().ToString(),
                        Createur = e.Createur
                    });
                }

                foreach (var e in ListeEvenementsRecents)
                {
                    viewModel.evenementsrecents.Add(new EvenementViewModel
                    {
                        EvenementId = e.EvenementId,
                        Nom = e.Nom,
                        Lieu = e.Lieu,
                        Date = e.Date,
                        Description = e.Description,
                        NbMembres = e.Membres.Count().ToString(),
                        Createur = e.Createur
                    });
                }

                return View(viewModel);
            }
        }

        // POST: /Groupe/AjouterGroupe
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AjouterEvenement(EvenementGlobalViewModel model)
        {
            using (var context = new ApplicationDbContext())
            {
                var UserId = User.Identity.GetUserId();
                var Createur = context.Users
                   .FirstOrDefault(user => UserId == user.Id);
                var evenement = new Evenement(model.AjouterNom, model.AjouterLieu, model.AjouterDescription, model.AjouterDate, Createur);
                evenement.Membres.Add(Createur);
                context.Evenements.Add(evenement);
                context.SaveChanges();
                return RedirectToAction("AfficherEvenements");
            }
        }

        public ActionResult SupprimerEvenement(int Id)
        {
            using (var context = new ApplicationDbContext())
            {
                var evenement = context.Evenements
                   .FirstOrDefault(e => Id == e.EvenementId);
                context.Evenements.Remove(evenement);
                context.SaveChanges();
                return RedirectToAction("AfficherEvenements");
            }
        }

        public ActionResult RejoindreEvenement(int Id)
        {
            using (var context = new ApplicationDbContext())
            {
                var evenement = context.Evenements
                    .Include("Membres")
                   .FirstOrDefault(e => Id == e.EvenementId);
                var UserId = User.Identity.GetUserId();
                var Createur = context.Users
                   .FirstOrDefault(user => UserId == user.Id);

                evenement.Membres.Add(Createur);
                context.SaveChanges();
                return RedirectToAction("AfficherEvenements");
            }
        }

        public ActionResult AnnulerEvenement(int Id)
        {
            using (var context = new ApplicationDbContext())
            {
                var evenement = context.Evenements
                    .Include("Membres")
                    .FirstOrDefault(e => Id == e.EvenementId);

                var UserId = User.Identity.GetUserId();
                var Createur = context.Users
                   .FirstOrDefault(user => UserId == user.Id);

                evenement.Membres.Remove(Createur);
                context.SaveChanges();
                return RedirectToAction("AfficherEvenements");
            }
        }
    }
}