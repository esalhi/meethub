﻿using MeetHub.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MeetHub.Controllers
{
    public class GroupeController : Controller
    {
        public ActionResult AfficherGroupes()
        {
            using (var context = new ApplicationDbContext())
            {

                var UserId = User.Identity.GetUserId();
                var ListeMesGroupes = context.Groupes
                    .Where(groupe => UserId == groupe.Createur.Id)
                    .Select(groupe => new
                    {
                        GroupeId = groupe.GroupeId,
                        Nom = groupe.Nom,
                        Lieu = groupe.Lieu,
                        Description = groupe.Description,
                        DateCreation = groupe.DateCreation,
                        Membres = groupe.Membres,
                        Evenements = groupe.Evenements,
                        Createur = groupe.Createur
                    });

                var ListeInvitationsGroupes = context.Groupes
                    .Where(groupe =>  groupe.Membres.Any(membre => membre.Id == UserId))
                    .Select(groupe => new
                    {
                        GroupeId = groupe.GroupeId,
                        Nom = groupe.Nom,
                        Lieu = groupe.Lieu,
                        Description = groupe.Description,
                        DateCreation = groupe.DateCreation,
                        Membres = groupe.Membres,
                        Evenements = groupe.Evenements,
                        Createur = groupe.Createur
                    });

                var ListeGroupesRecents = context.Groupes
                    .Where(groupe => !groupe.Membres.Any(membre => membre.Id == UserId))
                    .Take(6)
                    .Select(groupe => new
                    {
                        GroupeId = groupe.GroupeId,
                        Nom = groupe.Nom,
                        Lieu = groupe.Lieu,
                        Description = groupe.Description,
                        DateCreation = groupe.DateCreation,
                        Membres = groupe.Membres,
                        Evenements = groupe.Evenements,
                        Createur = groupe.Createur
                    });
            

                ViewBag.Message = "Groupes";
                var viewModel = new GroupeGlobalViewModel();
                foreach(var g in ListeMesGroupes)
                {
                    viewModel.groupes.Add(new GroupeViewModel {
                        GroupeId = g.GroupeId,
                        Nom = g.Nom,
                        Lieu = g.Lieu,
                        Description = g.Description,
                        DateCreation = g.DateCreation.Date.ToString("D"),
                        NbMembres = g.Membres.Count().ToString(),
                        NbEvenements = g.Evenements.Count().ToString(),
                        Createur = g.Createur
                    });
                }

                foreach (var g in ListeInvitationsGroupes)
                {
                    viewModel.invitations.Add(new GroupeViewModel
                    {
                        GroupeId = g.GroupeId,
                        Nom = g.Nom,
                        Lieu = g.Lieu,
                        Description = g.Description,
                        DateCreation = g.DateCreation.Date.ToString("D"),
                        NbMembres = g.Membres.Count().ToString(),
                        NbEvenements = g.Evenements.Count().ToString(),
                        Createur = g.Createur
                    });
                }

                foreach (var g in ListeGroupesRecents)
                {
                    viewModel.groupesrecents.Add(new GroupeViewModel
                    {
                        GroupeId = g.GroupeId,
                        Nom = g.Nom,
                        Lieu = g.Lieu,
                        Description = g.Description,
                        DateCreation = g.DateCreation.Date.ToString("D"),
                        NbMembres = g.Membres.Count().ToString(),
                        NbEvenements = g.Evenements.Count().ToString(),
                        Createur = g.Createur
                    });
                }

                return View(viewModel);
            }
        }

        // POST: /Groupe/AjouterGroupe
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AjouterGroupe(GroupeGlobalViewModel model)
        {
            using (var context = new ApplicationDbContext())
            {
                var UserId = User.Identity.GetUserId();
                var Createur = context.Users
                   .FirstOrDefault(user => UserId == user.Id);
                var groupe = new Groupe(model.AjouterNom, model.AjouterLieu, model.AjouterDescription, Createur);
                groupe.Membres.Add(Createur);
                context.Groupes.Add(groupe);
                context.SaveChanges();
                return RedirectToAction("AfficherGroupes");
            }
        }

        public ActionResult SupprimerGroupe(int Id)
        {
            using (var context = new ApplicationDbContext())
            {
                var groupe = context.Groupes
                   .FirstOrDefault(g => Id == g.GroupeId);
                context.Groupes.Remove(groupe);
                context.SaveChanges();
                return RedirectToAction("AfficherGroupes");
            }
        }

        public ActionResult RejoindreGroupe(int Id)
        {
            using (var context = new ApplicationDbContext())
            {
                //On recupere le groupe
                var groupe = context.Groupes
                    .Include("Membres")
                   .FirstOrDefault(g => Id == g.GroupeId);
                //On recupere l'utilisateur courant
                var UserId = User.Identity.GetUserId();
                var Createur = context.Users
                   .FirstOrDefault(user => UserId == user.Id);

                //On l'ajoute à la liste des membres du groupe
                groupe.Membres.Add(Createur);
                context.SaveChanges();
                return RedirectToAction("AfficherGroupes");
            }
        }

        public ActionResult QuitterGroupe(int Id)
        {
            using (var context = new ApplicationDbContext())
            {
                //On recupere le groupe
                var groupe = context.Groupes
                    .Include("Membres")
                   .FirstOrDefault(g => Id == g.GroupeId);
                //On recupere l'utilisateur courant
                var UserId = User.Identity.GetUserId();
                var Createur = context.Users
                   .FirstOrDefault(user => UserId == user.Id);

                //On le retire à la liste des membres du groupe
                groupe.Membres.Remove(Createur);
                context.SaveChanges();
                return RedirectToAction("AfficherGroupes");
            }
        }
    }
}