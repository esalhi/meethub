﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MeetHub.Models
{
    public class Evenement
    {
        public string Nom { get; set; }
        public string Lieu { get; set; }
        public DateTime DateCreation { get; set; }
		public DateTime DateEvent { get; set; }
        public List<Utilisateur> Membres { get; set; }
        public Groupe Groupe { get; set; }
        public Utilisateur Createur { get; set; }

        public Evenement(string nom, string lieu)
        {
            this.Nom = nom;
            this.Lieu = lieu;
            this.DateCreation = new DateTime();
			this.DateEvent = new DateTime();
            this.Membres = new List<Utilisateur>();
            this.Groupe = new Groupe("groupe anonyme", "Niger");
            this.Createur = new Utilisateur( "jln");
        }

        public Evenement(string nom, string lieu, List<Utilisateur> membres)
        {
            this.Nom = nom;
            this.Lieu = lieu;
            this.DateCreation = new DateTime();
            this.DateEvent= new DateTime();
            this.Membres = membres;
            this.Groupe = new Groupe("groupe anonyme", "Niger");
            this.Createur = new Utilisateur("jln");
        }

        public Evenement(string nom, string lieu, DateTime date, DateTime date_event, List<Utilisateur> membres)
        {
            this.Nom = nom;
            this.Lieu = lieu;
            this.DateCreation = date;
			this.DateEvent = date_event;
            this.Membres = membres;
            this.Groupe = new Groupe("groupe anonyme", "Niger");
            this.Createur = new Utilisateur("jln");
        }

        public Evenement(string nom, string lieu, DateTime date, DateTime date_event, List<Utilisateur> membres, Groupe groupe, Utilisateur createur)
        {
            this.Nom = nom;
            this.Lieu = lieu;
            this.DateCreation = date;
            this.DateEvent = date_event;
            this.Membres = membres;
            this.Groupe = groupe;
            this.Createur = createur;
        }


    }

}
