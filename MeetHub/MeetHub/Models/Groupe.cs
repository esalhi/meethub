﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;

namespace MeetHub.Models
{
    public class Groupe
    {
        [Key]
        public int GroupeId { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required]
        public string Lieu { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DateCreation { get; set; }
        public ICollection<ApplicationUser> Membres { get; set; }
        public ICollection<Evenement> Evenements { get; set; }
        public ApplicationUser Createur { get; set; }

        public Groupe()
        {
            this.DateCreation = new DateTime();
            this.Membres = new List<ApplicationUser>();
        }

        public Groupe(string nom, string lieu, string description, ApplicationUser createur)
        {
            this.Nom = nom;
            this.Lieu = lieu;
            this.Description = description;
            this.DateCreation = DateTime.Now;
            this.Membres = new List<ApplicationUser>();
            this.Evenements = new List<Evenement>();
            this.Createur = createur;
        }
    }
}
