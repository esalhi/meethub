﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace MeetHub.Models
{
    public class Evenement
    {
        [Key]
        public int EvenementId { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required]
        public string Lieu { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
		public string Date{ get; set; }
        public ICollection<ApplicationUser> Membres { get; set; }
        public ApplicationUser Createur { get; set; }

        public Evenement()
        {

        }

        public Evenement(string nom, string lieu, string description, string date, ApplicationUser createur)
        {
            this.Nom = nom;
            this.Lieu = lieu;
            this.Description = description;
            this.Date = date;
            this.Membres = new List<ApplicationUser>();
            this.Createur = createur;
        }
    }

}
